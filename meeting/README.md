
# 会议或直播安排与记录

该目录用于记录 “RISC-V Linux 内核兴趣小组” 的会议或直播分享记录和安排。

## 时间安排

如果没有特别的说明，会议和直播安排都是 **每周六晚上**：

* 会议时间：8:00 PM - 8:30 PM
* 直播时间：8:30 PM - 9:30 PM

## 会议地址

会议和直播均采用腾讯会议软件。

- 会议主题：RISC-V Linux 内核技术分享会
- 会议时间：2022/03/19-2022/06/04 20:30-21:30, 每周 (周六) 
- 参会方式
    - 点击链接入会： <https://meeting.tencent.com/dm/vAyxMqJPwkyd>
    - 腾讯会议入口：970-916-265
    - 会议直播：<https://meeting.tencent.com/l/lUeFE68dRAAR>

## 直播计划

### 回放发布

回放发布在如下三个视频频道：

- [泰晓学院](https://www.cctalk.com/m/group/90251209)
- [B 站频道](https://space.bilibili.com/687228362/channel/collectiondetail?sid=273934)
- [知乎专栏](https://www.zhihu.com/column/tinylab)

### 分享安排

部分未确定时间的分享还处在预约状态，具体分享时间待定，先完成的先做分享。

#### TODO

- 如何往新的处理器架构移植 Linux 内核 @通天塔 
    - 结合 RISC-V 源码和正在翻译的相关文章，做 Linux kernel porting 的分享，对照源码结构，介绍关键步骤和注意事项，并给出对应的源码实现文件和目录结构

- RISC-V 内核基础函数与汇编库分析 @郭天佑TanyoKwok

- RISC-V StackTrace 原理与实现 @Jeff

- 微架构优化与验证 @lzufalcon
    - 以 Ftrace & Tracepiont 为例，介绍内核 JIT 思想，优化原理，并通过分析汇编和二进制确认最终优化效果

- 不同架构基础指令的性能测试与数据分析 @hev @iosdevlog @dlan17 
    - 设计测试案例实测不同架构上的多个基础指令的性能数据并分析数据背后的处理器设计差异（欢迎CPU设计人员参与）

- 内核运行时代码修改的原理与实现 @pwl999 
    - 结合 Ftrace 与 Tracepoint 源码分析如何在运行时修改内核代码以及可能要注意的事项，包括多核竞争与内存管理等

### 已完成

- 20220416：RISC-V 指令编码实例 @lzufalcon
    - 以 Jump Label 为例，介绍如何参考 ISA 手册并编码 RISC-V 的 nop 与 jal 指令
    - 已上传视频剪辑

- 20220409：RISC-V rustSBI 知识分享 @luojia
    - 分享 RISC-V SBI 引导固件相关知识
    - 已上传视频剪辑

- 20220402：RISC-V Atomics 知识分享 @pingbo
    - 分享 RISC-V Atomics 指令和相关内核实现
    - 已上传视频剪辑

- 20220326：RISC-V ISA 简介 @iosdevlog
    - RISC-V ISA 规范介绍和演示
    - 已上传视频剪辑

- 20220319：RISC-V Paging&MMU 知识分享 @pwl999
    - 比照 64 位 X86 介绍 RISC-V Paging&MMU
    - 已上传视频剪辑

## 会议记录

### 20220423：第七周

经过连续不间断的 6 周线上分享后，由于大家比较忙，本周暂停线上分享，预计会在五一后恢复。

本周纪要：

* iosdevlog：认领 earlycon 和 dts。
* tjytimi：认领 Task Implementation 并启动分析过程。
* @通天塔：完成 Linux Porting 系列第 3 篇译稿并 Merge。
* @Jack Y：更新 Sparsemem，按 @lzufalcon Review 建议新增多幅图片。
* @lzufalcon：Review & Merge Linux Porting Part3；Merge Sparsemem。
* @tinylab：在多个渠道发布 Linux Porting part3。

### 20220416：第六周

本周由吴老师分享了 RISC-V Linux Tracepoint/Jump Label 的前两部分，即 Jump Label 核心原理介绍与 RISC-V 指令编码实战。演示和讨论视频稍后上传，见本文 “回放发布” 一节链接。

Jump Label 技术是内核中的经典类 “JIT” 思想，可以根据需要动态生成并替换指令，从而提升系统性能，确保 Tracepoint 等技术可以直接编译进内核，按需在线上启停，大大提升了 Linux Tracing 的可用性。

吴老师结合真实的性能数据详细讲解了其核心原理，之后介绍了如何参考 RISC-V 指令手册逐个编码 Jump Label 需要用到的两条基础指令，最后演示了通过 objdump、gdb 以及 pwn asm 三种工具实现快速指令编码的技巧。

期间，有同学介绍了 RISC-V Static PIE （静态编译 + 位置无关运行）的开发进展，讨论了 RISC-V 16/32 指令混排（RVI 基础指令与 RVC 扩展如何同时开启）与 ARM 16/32 指令混排（ARM 与 Thumb 指令集如何混排）等内容。

本周纪要：

* @cursor_risc-v：认领 Barriers。
* @davidlamb：认领 setup_vm。
* @sunnybird：认领 Security -> Penglai for RISC-V。
* @jacob：提交第 4 版 UEFI 相关文章并 Merge。
* @pingbo：研究 RISC-V SMP 动流程，特权指令，以及和 ARM 多核启动的对比。
* @通天塔：认领 Linux Porting 系列第 3 篇。
* @pwl999：研究 D1 在 RTOS 中使用 tspend 方式进行进程切换的原理。
* @Jack Y：提交并 Merge Sparsemem 一文，新增认领 Context Switch。
* @iosdevlog：调试 RVB2601RISC-V 生态开发板，比较 ARM 和 RISC-V；完善 microbench for ARMv7 支持。
* @lzufalcon：提交并 Merge Jump Label 系列第 3 篇核心实现分析，开展本周技术直播分享；完善 Linux Lab 中的 RISC-V 汇编例子；Review Sparsemem 与 UEFI v4 等多笔 PR；订正历史文章。
* @tinylab：协助剪辑本周视频；并在社区网站、B站、知乎、Cctalk、公众号、视频号、知识星球等多个渠道发布相关文章和视频；发布过程中协助订正多篇历史文章的个别错别字；协助更新 plan。

### 20220409：第五周

本周邀请了 RustSBI 作者洛佳老师做了 “RISC-V 引导程序环境：应用&规范” 的主题分享。演示和讨论视频稍后上传，见本文 “回放发布” 一节链接。

SBI 是 RISC-V 定义的一套 Supervisor Binary Interface，规范了 OS 调用 Machine Mode 服务的方式，而 RustSBI 是 SBI 标准的 Rust 语言实现，是 [RISC-V SBI](https://github.com/riscv-non-isa/riscv-sbi-doc/blob/master/riscv-sbi.adoc#49-sbi-implementation-ids) 规范标准中收录的实现之一。截止到分享当天，RustSBI 已支持 SBI 标准 v1.0，而另外一个 C 语言实现的 OpenSBI 仅支持 SBI v0.2（SBI v1.0 之前还有一个 v0.3）。

本周纪要：

* @Alony.Quan：认领 XIP。
* @Luo Xiaogang：认领 Ftrace。
* @zhao305149619：认领 Kexec & Kdump。
* @通天塔：提交并合并第 2 篇 Linux porting 译文，认领该系列最后 1 篇的翻译任务。
* @iosdevlog：为 microbench 适配 aarch64 和 armv7，并提交相应测试结果。
* @pwl999：在知识星球内分享了一个 RISC-V 逆向工具 ghidra。
* @pingbo：继续研究 SMP 相关功能，已经收集了一些资料。
* @jacob：提交第 3 版 UEFI 相关文章，增补了 OpenSBI 和 U-boot 相关知识，正在根据 Review 意见修订。
* @liaoyu：提交了第 1 版 RISC-V timer 相关分析文章，正在根据 Review 意见修订。
* @TanyoKwok：提交了第 1 版 Syscall 相关分析文章，正在根据 Review 意见修订。
* @lzufalcon：Review @iosdevlog microbench 新增架构代码；详细 Review 译文、UEFI、Timer 以及 Syscall 等数篇文章。
* @tinylab：协助剪辑视频并在各个渠道发布相关文章和视频。

### 20220402：第四周

本周由小文老师分享了原子指令及其用法。演示和讨论视频稍后上传，见本文 “回放发布” 一节链接。

有多位芯片设计与验证工程师和内核工程师参与了讨论，内容精彩，气氛热烈。

本周纪要：

* @pingbo：准备和开展了本次分享。
* @通天塔：第 2 篇翻译稿已提交 PR，@lzufalcon 和 @iosdevlog 完成了 Review。
* @iosdevlog：提交了 RISC-V ISA 介绍与演示 一文；在 D1 上运行 microbench 并提交了测试结果。
* @Jack Y,@liuxig,@liaoyu,@Stan Wang：分别认领了不同的任务，见 plan/README.md
* @lzufalcon：撰写了 jump label part2；撰写了 RISC-V Linux Distributions 一文；继续迭代 microbench 并撰写了首份 microbench 测试报告。
* @tinylab：协助剪辑视频并在各个渠道发布相关文章和视频。

### 20220326：第三周

本周分享由小贾老师主讲，主要介绍了 RISC-V ISA 相关的知识并做了简要演示。演示和讨论视频稍后上传，见本文 “回放发布” 一节链接。

会后大家进行了热烈的讨论，涉及本次活动的实验环境、任务认领、任务列表、UEFI、如何入门等议题，会前吴老师特别介绍了 RISC-V Linux 活动过程中提交成果时的注意事项。

**今天的分享非常适合有兴趣参加 RISC-V Linux 兴趣小组的同学们，在正式参与前建议先简单看一下。**

本周纪要：

* @通天塔：第 2 篇翻译稿已基本完成，准备提交 PR for Review。
* @pwl999：正在分析内核运行时代码修改相关内存管理部分。在星球分享了 RISC-V RTOS 相关资料。
* @pingbo：已经提交第 1 篇 Atomic Locking 相关的技术文章，merged。
* @iosdevlog：准备和开展本周分享。
* @jeff.zhao：第 2 次提交 StackTrace 一文，merged。
* @jacob：提交了 UEFI 第1篇文章，reviewed，等待修订后再 merge。
* @lzufalcon：完善 microbench 测试框架并提交了 x86_64 平台的测试代码和测试数据，提交了 riscv64 测试代码；基于 Linux v5.17 录制了 RISC-V Linux 内核实验教程。
* @dlan17：测试并提交了 Unmatched 这款 RISC-V 平台的 microbench 数据，另外也新增了一笔 PMU 任务。
* @tinylab：协助剪辑视频并在各个渠道发布相关文章和视频。

### 20220319：首次分享

本次分享由彭伟林老师主讲，比照 X86 介绍了 RISC-V Paging & MMU，并展开介绍了内核相关的公共部分。次日已剪辑并发布到三个视频频道，见本文 “回放发布” 一节链接。

本周纪要：

* @通天塔：已经提交 Linux Porting to New Arch 系列文章第 1 篇的译稿，正在翻译剩下的 2 篇。
* @pwl999：开展本周分享，后续继续分析 psABI，并协助 @lzufalcon 分析内核运行时代码修改相关内存管理部分，比如 `patch_map/patch_unmap`。
* @pingbo：继续阅读 ISA 部分，准备第 3 场分享并开始分析 Atomic Locking 部分。在星球分享了 RISC-V ISA 相关资料。
* @iosdevlog：准备第 2 场分享。
* @郭天佑TanyoKwok：认领 Generic library routines and assembly
* @lzufalcon：校订 @通天塔 的译文；发布 Jump Label 源码分析第 1 节，接下来继续撰写另外 2 篇文章，并基于 21 日发布的 Linux v5.17 录制 RISC-V Linux 内核实验教程。
* @hev,@pingbo,@dlan17：协同设计测试用例，测试不同架构基础指令的性能，部分结果已经整理到 Jump Label 分析第 1 节，接下来继续完善测试用例，新增 D1 和 Unmatched 两个 RISC-V 平台的数据。
* @tinylab：协助剪辑视频并在各个渠道发布相关文章和视频。

### 20220312: 首次周会

本次周会回顾了上周的动态并制定了下周计划，下周开始轮流作视频分享，分享时间从 `8:30-9:30`。

本周纪要：

* @pwl999：本周已经收集和整理了部分 MM 的资料，计划下周六（20220319）做直播分享，接下来计划分析 ABI 部分。在星球分享了 T-head 相关资料。
* @pingbo：本周正在学习相关 Spec，初步了解 RISC-V Linux 启动过程，计划 20220402 做直播分享，接下来计划横向对比 RISC-V 和其他架构，分析 RISC-V 关键特点，远期计划移植一款 rtos 到 RISC-V。
* @iosdevlog：正在学习 RVOS 并购买了相关书籍，正在整理 RISC-V Spec 相关资料，计划于 20220326 做分享，接下来有意向调研一下开源的 RISC-V 核，尝试用 FPGA 跑起来。
* @lzufalcon: 调研了最新 RISC-V Linux 社区动态，发布了兴趣小组招募信息，并撰写了首篇分析文章，同时分享了如何用 Linux Lab Disk 开展 RISC-V 汇编语言实验，下周继续介绍如何用 Linux Lab Disk 开展 RISC-V Linux 内核实验。

另有多位同学参与了讨论和交流：

* 一位同学认领了 "Porting Linux to a new processor architecture" 的翻译部分。
* 另外一位同学咨询了开发与测试环境部分，这部分我们统一采用 Linux Lab + riscv64/virt 或 D1 开发板。
* 还有一位同学建议是否可以从 Fixup 的角度来参与，我们把招募信息中收集的 RISC-V Linux 最近三个月的关键动态追加到了 plan/README.md，后续大家也可以认领。

### 20220305：项目启动会

本次会议正式启动了 “RISC-V Linux内核兴趣小组”，讨论了首期 3 个月的目标：完成 Linux RISC-V 架构相关部分的剖析，并现场创建了 RISC-V Linux 协作仓库。

本周纪要：

* @pwl999：认领了 Paging & MMU。
* @iosdevlog：认领了 ISA，后续兼任项目会议组织和进度管理。
* @pingbo：认领了 Atomic and Locking Code。
* @lzufalcon: 创建了 RISC-V Linux 协作仓库，整理了 riscv-linux.xmind 以及相关资料，初步整理了任务列表。
