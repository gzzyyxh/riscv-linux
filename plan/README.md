
# Tasks

本文用于汇总 RISC-V 的相关任务和图文视频等分析成果。

大家可在相应任务后追加 `@your-gitee-id` 来认领任务，也可以提交 PR 新增或修订任务。

相关图文或直播回放成果在发布后将于第一时间更新进来。

## QuickStart

* Howto
    * [如何分析 Linux 内核 RISC-V 架构相关代码](https://tinylab.org/riscv-linux-quickstart/) @lzufalcon

* RISC-V Linux Distributions
    * [两分钟内极速体验 RISC-V Linux 系统发行版](https://tinylab.org/riscv-linux-distros/) @lzufalcon

* Pull Request
    * [直播回放：提交 PR 须知 —— 中英文排版等](https://www.cctalk.com/v/16484083661860) @lzufalcon

## Experiments

* Assembly
    * [视频演示：快速上手 RISC-V 汇编语言实验](https://www.cctalk.com/v/16484116944868) @tinylab

* RISC-V OS Basics
    * [视频演示：无门槛开展 RVOS 课程实验](https://www.cctalk.com/v/16484116944869) @tinylab

* Linux Kernel
    * [视频演示：极速开展 RISC-V Linux v5.17 内核实验](https://www.cctalk.com/v/16484095223067) @tinylab

## Hardware

* D1 Board
    * [D1-H 开发板入门](https://tinylab.org/nezha-d1-quickstart/) @iosdevlog
    * [为哪吒 D1 开发板安装 ArchLinux RISC-V rootfs](https://tinylab.org/nezha-d1-archlinux/) @tieren

* Longan Nano
    * [在 Linux 下制作 rv-link 调试器](https://tinylab.org/rv-link-debugger/) @tieren

## Specs

* ISA
    * [RISC-V ISA 简介](https://tinylab.org/riscv-isa-intro), [幻灯](https://gitee.com/tinylab/riscv-linux/raw/master/ppt/riscv-isa.pptx), [直播回放](https://www.cctalk.com/v/16484079493695) @iosdevlog

* psABI @pwl999
* SBI
* Extensions

## Basic support

* Build Infrastructure
* User-facing API
* Generic library routines and assembly @郭天佑TanyoKwok
    * FPU
    * non FPU
    * [non M-extension](https://lore.kernel.org/linux-riscv/20220402101801.GA9428@amd/T/#t)
    * futex

## Bootloader

* BBL (Berkeley Boot Loader)
* OpenSBI
* RustSBI
    * RISC-V 引导程序环境：应用&规范，[幻灯](https://gitee.com/tinylab/riscv-linux/raw/master/ppt/riscv-rustsbi.pdf)，[直播回放](https://www.cctalk.com/v/16495466863205) @luojia65
* U-Boot
* UEFI

## Boot, Init & Shutdown

* Booting
* DTS @iosdevlog
* UEFI support @jiangbo.jacob
* Compressed images
* Early Boot @ze_zhang
* earlycon @iosdevlog
* Soc Init
* setup_arch
* poweroff

## Memory Management

* Paging and MMU
    * [RISC-V MMU 概述](https://tinylab.org/riscv-mmu/), [直播回放](https://www.cctalk.com/v/16477623213273) @pwl999

* setup_bootmem
* setup_vm @davidlamb
* fixed mapping
* ioremap
* Barriers   @cursor_risc-v
* Sparsemem @尹天宇
* CMA @huozhikun
* DMA
* Hugetlbfs
* memtest
* NUMA
* nommu

## Device, IRQs, Traps and the SBI

* SBI
* Device
* IRQs
* Traps
* swiotlb

## Timers

* Timers @nalan01
* swtimer
* udelay @stanwang9211

## Synchronization

* Atomics
    * [RISC-V 原子指令与用法介绍](https://tinylab.org/riscv-atomics/), [直播回放](https://www.cctalk.com/v/16489499392383) @pingbo

* [Ticket Spinlock](https://lore.kernel.org/linux-riscv/11364105.8ZH9dyz9j6@diego/T/#t)
* [Restartable Sequence (RSEQ)](https://lore.kernel.org/linux-riscv/20220308083253.12285-1-vincent.chen@sifive.com/T/#t)

## Multi-core

* SMP support
* IPI
* Hotplug
* CPUIdle

## Scheduling

* Context switch @尹天宇
* Task implementation @tjytimi 
* Multi-tasking @Jack Y.

## User-space Support

* Syscall
* ELF support
* module implementation
* Multi-threads
* binfmt_flat

## Tracing

* Stacktrace
    * [RISC-V Linux Stacktrace 详解](https://tinylab.org/riscv-stacktrace/) @zhao305149619

* Tracepoint
* Ftrace @luoxiaogang
* Kprobes
* Uprobes
* eBPF @Yuandaodao

## Debugging

* Ptrace @liuxig
* Crash
* KGDB
* Kexec & Kdump @zhao305149619

## Detection

* KCSAN
* KASAN
* KFENCE
* Kmemleak

## Profiling

* Perf
* PMU (Performance Monitor Unit)
* kcov

## Tuning

* vdso
* Jump Label
    * [RISC-V Linux jump_label 详解，第 1 部分：技术背景](https://tinylab.org/riscv-jump-label-part1/) @lzufalcon
    * [RISC-V Linux jump_label 详解，第 2 部分：指令编码](https://tinylab.org/riscv-jump-label-part2/), [直播回放](https://www.cctalk.com/v/16501801841966) @lzufalcon

* Static call @lzufalcon

## Security

* Stackprotector
* OP-TEE for RISC-V
* PengLai for RISC-V @jinlongfeng

## Virtulization

* [KVM](https://lore.kernel.org/linux-riscv/CAAhSdy3JwLyOoNOubAS2VusNdj6O-CJJNSs+mM8+NvKErtAdmw@mail.gmail.com/T/#u)

## Fast boot

* XIP @alony-quan

## Real Time

* Preemption

## Latest development

From: <https://lore.kernel.org/linux-riscv/>

* Add Sstc extension support (KVM related timer support)
* RISC-V IPI Improvements (Multi-core support)
* Fixes KASAN and other along the way (KASAN support)
* RISCV_EFI_BOOT_PROTOCOL support in linux (UEFI boot support)
* Risc-V Svinval support (for the Svinval v1.0 defined in Privileged specification)
* riscv: compat: Add COMPAT mode support for rv64
* Provide a fraemework for RISC-V ISA extensions
* Improve RISC-V Perf support using SBI PMU and sscofpmf extension (Perf support)
* riscv: compat: vdso: Add rv32 VDSO base code implementation (VDSO support)
* RISC-V CPU Idle Support (cpuidle support)
* KVM RISC-V SBI v0.3 support (KVM & SBI)
* KVM/riscv fixes for 5.17, take #1 (KVM)
* Add Sv57 page table support (MM Paging, try from 5-level page table to 3-level page table)
* Introduce sv48 support without relocatable kernel
* unified way to use static key and optimize pgtable_l4_enabled
* riscv: support for Svpbmt and D1 memory types ("Supervisor-mode: page-based memory types" for things like non-cacheable pages or I/O memory pages.)
* riscv: fix oops caused by irqsoff latency tracer (Ftrace irqsoff tracer)
* riscv: cpu-hotplug: clear cpu from numa map when teardown (Multi-core support: cpu hotplug)
* Improve KVM's interaction with CPU hotplug (KVM and CPU hotplug)
* riscv: add irq stack support (kernel stack max size)
* Sparse HART id support (hartid (core id) v.s. NR_CPUS)
* RISC-V: Prevent sbi_ecall() from being inlined (standard deviate)
* Public review of Supervisor Binary Interface (SBI) Specification (what SBI is and v1.0-rc2 is in review)
* riscv: switch to relative extable and other improvements (extable)
* riscv: Fixes for XIP support (XIP)
* riscv: bpf: Fix eBPF's exception tables (eBPF)
* RISC-V: Use SBI SRST extension when available (SBI)
* Public review for RISC-V psABI v1.0-rc1 (psABI)
* riscv: Add vector ISA support (vector ISA)
