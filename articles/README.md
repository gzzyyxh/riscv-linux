
# 文字成果

该目录用于存放 RISC-V Linux 内核剖析 专项活动的文字性输出成果。

## 中文排版

为了更好展示输出成果，请大家在提交成果之前查阅一下 [中文文案排版指北](https://github.com/sparanoid/chinese-copywriting-guidelines/blob/master/README.zh-Hans.md)，可参考如下案例分享：

* [直播回放：提交 PR 须知 —— 中英文排版等](https://www.cctalk.com/v/16484083661860)

## 校订须知

由于惯性思维，撰写文章后很难通过自身校订完全订正，所以大都需要有一个其他同学帮助 Review 的流程。

为了提高 Review 效率，减轻校订的工作量，约定如下：

* 提交 Pull Request 前后
    * 请务必先遵守中文排版中的约定，这块不复杂，仔细一些都可以自己校订好。
    * 在排版之外，还可以更通顺，避免堆砌；让结构清晰有条理，避免混乱无章；补充权威资料和规范；清晰说明源码版本、路径甚至代码。
    * 作者提交 Pull Request 后，可以在协作群通知其他同学，可以是任何你希望邀请 Review 的同学。

* 其他人在校订过程中
    * 一般需要检查错别字、排版、链接有效性以及表述是否有误等。
    * 校订过程繁重，欢迎更多人参与校订，校订不仅可以提前阅读文章，也可以通过发现并指出文中错误来提高技术能力和文章撰写水平。

* 校订过程的原子性
    * 请作者在校订完后才开始文章的修改，以避免遗漏校订人员中途新增的 Review 意见。
    * 校订人员 Review 完后，可以在 PR 评论区或协作群联系作者说明已经完成 Review。

* 基于校订信息进行修改
    * 首次提交 Pull Request 后，不可避免的，可能会收到很多 Review 意见，请保持耐心。
    * 为避免漏掉 Review 意见而造成反复，建议采取如下措施：
        * 对于不认可的意见，可以写上 rejected，并注明原因；
        * 对于认可的意见，可以写上 applied；
        * 在准备 2 次 PR 之前，检查是否还有未标记过的 Review 意见。

另外，已经有大量的 [校订案例](https://gitee.com/tinylab/riscv-linux/pulls?project_id=tinylab%2Friscv-linux&sort=closed_at+desc&status=closed)，作者查阅可以避免犯同样的错误，校订人员查阅可以看看校订思路。

## 文件名

为了避免编码的不一致，文件名统一以 `日期-英文标题.md` 的方式命名，英文标题内如果有空格，替换为 `-` 号，例如：

```
20220308-riscv-linux-quickstart.md
```
## 文件头说明

新撰写的文章，统一文件头如下：

```
> Author:  678 <xxx@aaa.com><br/>
> Date:    2022/0x/0y<br/>
> Revisor: ABC <xxx@yyy.com><br/>
> Project: [RISC-V Linux 内核剖析](https://gitee.com/tinylab/riscv-linux)
```

翻译的文章，统一文件头如下：

```
> Title:      Porting Linux to a new processor architecture, part 1: The basics<br/>
> Author:     Joël Porquet<br/>
> Translator: 123 <xxx@zzz.com><br/>
> Date:       2022/0x/0y<br/>
> Revisor:    ABC <xxx@yyy.com><br/>
> Project:    [RISC-V Linux 内核剖析](https://gitee.com/tinylab/riscv-linux)
```

## 翻译格式

为了方便校订和读者交叉确认，所有翻译文章采用英文原文和中文翻译整段对照的方式，章节小标题采用 `翻译（原标题）` 的方式展示，具体请参考：

* [LWN 531148: Linux 内核文件中的非常规节](https://tinylab.org/lwn-531148/)
